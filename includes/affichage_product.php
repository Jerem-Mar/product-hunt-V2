<?php 
    //On récupére tout le contenu de la table produit
    $reponse = $bdd->query('SELECT * FROM Produit');

    //$likes = $bdd->query('SELECT id_produit, COUNT(id_produit) FROM likes GROUP BY id_produit');
    //On affiche chaque entrée une à une
    while ($produit = $reponse->fetch())
    {
        ?>

<div class="list-group-item list-group-item-action" data-toggle="modal" data-target="#Modal<?php echo $produit['id']; ?>">

  <div class="container contZindex">

    <div class="row ">

      <div class="col-12 col-lg-3 col-md-2  text-center logZindex">
        <img src="<?php echo $produit['logo']; ?>" width="100" height="100" alt="logo">
        <!--logo-->
      </div>
      <div class="col-12 col-lg-6 col-md-6">
        <div class="list-group list-group-action flex-column align-items-start descZindex">
          <a class="lien" href="<?php echo $produit['lien_site']; ?>">
            <h4 class="mb-1">
              <?php echo $produit['nom_produit']; ?>
            </h4>
            <!--description produit-->
            <p class="text-muted">
              <?php echo $produit['mini_description']; ?>
            </p>
          </a>
        </div>
      </div>

      <div class="col-12 col-lg-3 col-md-4 align-self-end text-right btnZindex">
        <!--boutons-->
        <button id="" type="button" class="btn btn-secondary like" data-id-produit="<?php echo $produit['id']; ?>">

          <?php include("affichage_like.php"); ?>
        </button>
        <a class="btn btn-secondary commentaire" href="pages/product.php?id_produit=<?php echo $produit['id']; ?>">
          💭
        </a>
      </div>

    </div>

  </div>

</div>


<!-- Modal -->

<?php include 'modal.php' ?>

<?php
    }

    $reponse->closeCursor(); //Termine le traitement de la requete
    ?>